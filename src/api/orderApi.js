import axiosClient from "./axiosClient";

const orderApi = {
  getOrders: ({size , page}) => {
    const url = `orders?page=${page}&size=${size}`;
    return axiosClient.get(url);
  },
  
};

export default orderApi;
