import axiosClient from "./axiosClient";

const userApi = {
  queryUser: ( {size, page} ) => {
      const url = `/users?role=admin&size=${size}&page=${page}`;
    return axiosClient.get(url);
  },
  getUserById: (id) => {
    const url = `users/${id}`;
    return axiosClient.get(url);
  },
  deleteUserById: (id) => {
    const url = `users/${id}`;
    return axiosClient.delete(url);
  },
};

export default userApi;
