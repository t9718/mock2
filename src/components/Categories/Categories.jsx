import { Box, Typography } from '@mui/material'
import React from 'react'
import productApi from '../../api/productApi'
import ListIcon from '@mui/icons-material/List';

function Categories() {
  productApi.getAllCategories().then(r => console.log(r))
  return (
    <Box sx={{width:'280px', height:'628px', background:'#3D464D'}}>
      <Box sx={{width:'280px', background:'pink', display:"flex", alignItems:'center'}}>
        <ListIcon fontSize='large' sx={{color:'white'}}/>
        <Typography sx={{
          fontFamily: 'Arial',
          fontStyle: 'normal',
          fontWeight: '700',
          fontSize: '32px',
          lineHeight: '37px',
          color: '#FFFFFF',
        }}>Categories</Typography>
      </Box>
    </Box>
  )
}

export default Categories