import { Box, Typography } from '@mui/material'
import React, { useContext, useState } from 'react'
import DashboardIcon from '@mui/icons-material/Dashboard';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import PersonIcon from '@mui/icons-material/Person';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import SettingsIcon from '@mui/icons-material/Settings';
import EqualizerIcon from '@mui/icons-material/Equalizer';

const DropDown = React.createContext();
const GetStatusActive = React.createContext();

function Logo(){
    return(
        <Box sx={{
          height: '78px',
          width: '225',
          borderRadius: '1px',
          background:'#FFD333',
          display:'flex',
          alignItems:'center',
          justifyContent:'space-around'
        }}>
          <Typography sx={{
              fontFamily: 'Red Rose',
              fontStyle: 'normal',
              fontSize: '25px',
              lineHeight: '31px',
              color: '#000000'
          }}>
            SHOP APP
          </Typography>
          <Box sx={{
              width:'53.7px',
              height: '18px',
              background: '#FFFFFF',
              borderRadius: '5px',
              display:'flex',
              alignItems:'center',
              justifyContent:'center'
          }}>
            <Typography sx={{
                fontFamily: 'Arial',
                fontStyle: 'normal',
                fontWeight: 700,
                fontSize: '10px',
                lineHeight: '11px',
                color: '#000000',
                marginTop: 0.5
          }}>
              ADMIN
            </Typography>
          </Box>
        </Box>
    )
}

function Button(props){
    const valueDrop = useContext(DropDown)
    const valueActive = useContext(GetStatusActive)
    const handelClick =(e)=>{
        if(props.Drop == true){
            valueDrop.upDateStatus(!valueDrop.DropDownStatus)
        } else {
            valueActive.setButtonActive(props.children)
        }
        console.log(e)
    }
    console.log(typeof props.Icon == 'object')
    return(
        <Box sx={{
            width:'225px', height:'55px', display:'flex', alignItems:'center', cursor:'pointer', background:valueActive.buttonActive==props.children?'#FFD333':'#3D464D', position: 'relative', zIndex:'100',
            '&:hover':{
                background:'#515D66'
            }}} onClick={handelClick}>
            <Box sx={{display:'flex', alignItems:'center', flex:'6'}}>
                <Box sx={{marginLeft:'10px', marginRight:'9px', color:'white', display:typeof props.Icon == 'object'?'flex':'none', justifyContent:'center', alignItems:'center'}}>
                    {props.Icon}
                </Box>
                <Typography sx={{
                    fontFamily: 'Arial',
                    fontStyle: 'normal',
                    fontWeight: 700,
                    fontSize: '18px',
                    lineHeight: '21px',
                    color: '#FFFFFF',
                    marginTop: 1,
                    marginLeft: typeof props.Icon == 'object'?'0px':'39px'
                }}>
                    {props.children}
                </Typography>
            </Box>
            {props.Drop ? (
                <Box sx={{flex:2, display:'flex', alignItems:'center', justifyContent:'center'}}>
                    <ArrowDropDownIcon fontSize='large' sx={{color:'white', marginTop: 1, transition: 'transform 0.5s', transform: valueDrop.DropDownStatus?'rotate(0deg)':'rotate(90deg)'}}/>
                </Box>
            ):(<></>)}
        </Box>
    )
}

function Drop(props) {
    const [status, upDateStatus] = useState(false)
    return(
        <DropDown.Provider value={{DropDownStatus: status, upDateStatus: upDateStatus}}>
            {props.MainButton}
            <Box sx={{position: 'relative', marginTop:status?'0px':'-110px', transition:'margin 0.5s', zIndex:`${99 - props.zIndex}`}}>
                {props.children}
            </Box>
        </DropDown.Provider>
    )
}

function Title() {
    return(
        <Box sx={{width:'225px', height:'55px', display:'flex', alignItems:'center', marginLeft:'10px'}}>
            <text style={{
                fontFamily: 'Arial',
                fontStyle: 'normal',
                fontWeight: 700,
                fontSize: '12px',
                lineHeight: '14px',
                color: '#C4C4C4'
            }}>
                APPLICATION
            </text>
        </Box>
    )
}

function AdminNavbar() {
  const [buttonActive, setButtonActive] = useState('')
  return (
    <GetStatusActive.Provider value={{buttonActive:buttonActive, setButtonActive:setButtonActive}}>
        <Box sx={{width:'225px', height:'100vh', background: '#3D464D'}}>
        <Logo/>
        <Title/>
        <Button Icon={<DashboardIcon/>}>Dashboard</Button>
        <Drop MainButton={<Button Icon={<EqualizerIcon/>} Drop>Product</Button>} zIndex={0}>
            <Button>Product List</Button>
            <Button>Add Product</Button>
        </Drop>
        <Drop MainButton={<Button Icon={<PersonIcon/>} Drop>User</Button>} zIndex={1}>
            <Button>User List</Button>
            <Button>Add User</Button>
        </Drop>
        <Button Icon={<ShoppingCartIcon/>}>Orders</Button>
        <Drop MainButton={<Button Icon={<SettingsIcon/>} Drop>Settings</Button>} zIndex={2}>
        </Drop>
        </Box>
    </GetStatusActive.Provider>
  )
}

export default AdminNavbar