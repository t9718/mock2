import { useEffect, useRef, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import productApi from "../../api/productApi";
import leftArrow from "../../media/left.png";
import rightArrow from "../../media/right.png";
import ProductCard from "../ProductCard/ProductCard";
import "./style.scss";

const RelatedProducts = ({ product }) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const sliderRef = useRef();

  const sliderSettings = {
    slidesToShow: 4,
    slidesToScroll: 2,
    infinite: true,
    arrow: false,
    useTransform: false,
  };
  console.log(product);
  console.log("data", data);

  useEffect(() => {
    productApi.getAll(product.category).then(({ data }) => {
      setData(data.result);
      setLoading(false);
    });
  }, []);

  return (
    <>
      <div className="related-products">
        <div className="related-products__heading">
          <h2 className="related-products__heading-name">Related Products</h2>
          <div className="related-products__heading-arrow">
            <img src={leftArrow} onClick={() => sliderRef.current.slickPrev()} />
            <img src={rightArrow} onClick={() => sliderRef.current.slickNext()} />
          </div>
        </div>
        <Slider {...sliderSettings} ref={sliderRef}>
          {!loading &&
            data
              .slice(0, 8)
              .map((item) => <ProductCard key={item.id} className="no-flick" data={item} />)}
        </Slider>
      </div>
    </>
  );
};

export default RelatedProducts;
