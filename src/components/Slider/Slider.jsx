import Slider from "react-slick";
import left from "../../media/left.png";
import right from "../../media/right.png";

const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
  <img src={left} alt="prevArrow" {...props} />
);

const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
  <img src={right} alt="nextArrow" {...props} />
);

const sliderSettings = {
  slidesToShow: 4,
  slidesToScroll: 1,
  infinite: true,
  prevArrow: <SlickArrowLeft />,
  nextArrow: <SlickArrowRight />,
};

const CustomSlider = () => {
  return (
    <Slider {...sliderSettings}>
      <div>
        <h3>1</h3>
      </div>
      <div>
        <h3>2</h3>
      </div>
      <div>
        <h3>3</h3>
      </div>
      <div>
        <h3>4</h3>
      </div>
      <div>
        <h3>5</h3>
      </div>
      <div>
        <h3>6</h3>
      </div>
    </Slider>
  );
};

export default CustomSlider;
