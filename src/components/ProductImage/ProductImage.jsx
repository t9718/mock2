import { useEffect, useState } from "react";
// import "react-image-gallery/styles/css/image-gallery.css";
import "./style.scss";

function ProductImage({ product }) {
  const [url, setUrl] = useState(null);

  useEffect(() => {
    if (product) {
      setUrl(product[0].url);
    }
  }, [product]);

  return (
    <div>
      <div className="product-img__thumbnail">
        <img src={url} />
      </div>
      <ul className="product-img__list">
        {product.length > 0 &&
          product.map((image) => (
            <img
              onMouseEnter={(e) => setUrl(e.target.src)}
              className={`product-img__item ${image.url === url && "product-img__item--active"}`}
              src={image.url}
            />
          ))}
      </ul>
    </div>
  );
}

export default ProductImage;
