import { Box, IconButton, Typography } from '@mui/material'
import React from 'react'
import ListIcon from '@mui/icons-material/List';
import SearchIcon from '@mui/icons-material/Search';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Avatar from '@mui/material/Avatar';
import Badge from '@mui/material/Badge';

function AdminHeader() {
  return (
    <Box sx={{background:'#FFFFFF', height:'78px', width:'100%',display:'flex', alignItems:'center', justifyContent:'space-between'}}>
        <Box sx={{display:'flex', alignItems:'center',marginLeft:'28px', }}>
            <ListIcon sx={{color:'black',marginRight:'30px'}} fontSize='large'/>
            <Box sx={{display:'flex'}}>
                <Box sx={{
                    display:'flex',
                    justifyContent:'center',
                    alignItems:'center',
                    background: '#E5E5E5',
                    borderRadius: '5px 0px 0px 5px',
                    width:'42px',
                    height: '42px',
                    color:'#929395'
                }}>
                    <IconButton>
                        <SearchIcon /> 
                    </IconButton>
                </Box>
                <input style={{
                    background: '#E5E5E5',
                    borderRadius: '0px 5px 5px 0px',
                    border:'none',
                    outline:'none',
                    width:'250px',
                    height: '42px'
                }}
                placeholder={'Search'}
                />
            </Box>
        </Box>
        <Box sx={{display:'flex', alignItems:'center',width:'220px',justifyContent:'space-between', marginRight:'28px'}}>
        <IconButton sx={{color:'black'}}>
                <Badge badgeContent={4} color="primary">
                    <NotificationsIcon fontSize='large'/>
                </Badge>
            </IconButton>
            <Avatar variant="rounded" src='https://upload.wikimedia.org/wikipedia/commons/8/85/Elon_Musk_Royal_Society_%28crop1%29.jpg'/>
            <Box>
                <Typography sx={{
                    fontFamily: 'Arial',
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: '18px',
                    lineHeight: '21px',
                    color: '#000000',
                }}>
                    Elon Musk
                </Typography>
                <Typography sx={{
                    fontFamily: 'Arial',
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: '15px',
                    lineHeight: '17px',
                    color: '#929395',
                }}>
                    admin    
                </Typography>
            </Box>
        </Box>
    </Box>
  )
}

export default AdminHeader