import { Avatar, Divider, Form, Input, Pagination, Rate } from "antd";
import { useRef } from "react";
import CustomButton from "../../CustomButton/CustomButton";
import "./style.scss";

const Reviews = ({ reviews, onFinish }) => {
  const inputRef = useRef(null);

  const finishHandler = (event) => {
    onFinish(event);
  };

  return (
    <>
      <div>
        <h1 className="reviews-title">Customer Reviews</h1>
        <ul className="reviews-list">
          {reviews.result.length > 0 &&
            reviews.result.map((review, index) => (
              <li key={index}>
                <div className="customer-review-container">
                  <div className="customer-review__avatar">
                    {review.userReview.avatar ? (
                      <img
                        src={review.userReview.avatar}
                        alt={review.userReview.name}
                        className="customer-review__avatar-img"
                      />
                    ) : (
                      <Avatar className="customer-review__avatar-img">
                        {review.userReview.username.charAt(0).toUpperCase()}
                      </Avatar>
                    )}
                  </div>
                  <div className="customer-review__content">
                    <div className="customer-review__name">{review.userReview.username}</div>
                    <Rate disabled defaultValue={review.rating} />
                    <div className="customer-review__comment">{review.content}</div>
                    <div className="customer-review__date">
                      {new Date(review.createdAt).toLocaleString("de-DE", {
                        day: "numeric",
                        month: "short",
                        year: "numeric",
                      })}
                    </div>
                  </div>
                </div>
                <Divider className="customer-review__divider" />
              </li>
            ))}
        </ul>
        <Pagination className="reviews-pagination" />
      </div>
      <div className="write-review">
        <h1 className="write-review__title reviews-title">Write Review</h1>
        <Form ref={inputRef} onFinish={finishHandler}>
          <Form.Item name="rate" rules={[{ required: true, message: "Please rate the product!" }]}>
            <Rate className="write-review__rating" />
          </Form.Item>
          <Form.Item
            name="review"
            rules={[{ required: true, message: "Please write your review!" }]}
          >
            <Input className="write-review__content" placeholder="Write Your Review..." />
          </Form.Item>
          <CustomButton>Post your Review</CustomButton>
        </Form>
      </div>
    </>
  );
};

export default Reviews;
