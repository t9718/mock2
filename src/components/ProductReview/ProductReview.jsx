import { useState } from "react";
import Description from "./Description/Description";
import Reviews from "./Reviews/Reviews";
import Specification from "./Specification/Specification";
import "./style.scss";

const ProductReview = ({ data }) => {
  const { reviews, product } = data;
  const [active, setActive] = useState(2);

  const handleReview = (event) => {
    console.log("post review");
    console.log("input: ", event);
  };

  const segmentData = [
    { name: "Description", content: <Description product={product} /> },
    { name: "Specification", content: <Specification product={product} /> },
    {
      name: "Reviews",
      content: <Reviews reviews={reviews} onFinish={handleReview} />,
    },
  ];

  return (
    <div>
      <div className="reviews">
        <div className="reviews-segment">
          <ul className="reviews-segment__nav">
            {segmentData.map((data, index) => (
              <li
                key={data.name}
                className={`reviews-segment__title ${active === index && "active"}`}
                onClick={() => setActive(index)}
              >
                {data.name}
              </li>
            ))}
            <div className="reviews-segment__indicator" />
          </ul>
        </div>

        <div className="segment-content">{segmentData[active].content}</div>
      </div>
    </div>
  );
};

export default ProductReview;
