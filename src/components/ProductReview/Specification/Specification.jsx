import "./style.scss";

const Specification = ({ product }) => {
  return (
    <table id="specification">
      <tbody>
        <tr>
          <td>Category</td>
          <td>{product.category}</td>
        </tr>
        <tr>
          <td>Name</td>
          <td>{product.name}</td>
        </tr>
        <tr>
          <td>Brand</td>
          <td>{product.brand}</td>
        </tr>
      </tbody>
    </table>
  );
};

export default Specification;
