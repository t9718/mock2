import "antd/dist/antd.min.css";
import React from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import AdminNavbar from "./components/AdminNavbar/AdminNavbar";
import Admin from "./features/Admin/Admin";
import Home from "./features/Home/Home";
import NavbarUser from "./features/NavbarUser/NavbarUser";
import ProductDetail from "./features/ProductDetail/ProductDetail";
import SearchResult from "./features/SearchResult/SearchResult";
import ShoppingCart from "./features/ShoppingCart/ShoppingCart";
import VerifyEmail from "./features/VerifyEmail/VerifyEmail";
import UserDetail from "./features/AdminUserDetail/UserDetail"
import UserList from "./features/AdminUserList/UserList";
import OrderList from "./features/AdminOrderList/OrderList";
import ProductList from "./features/AdminProductList/ProductList";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<NavbarUser />}>
          <Route index element={<Home />} />
          <Route path="product/:id" element={<ProductDetail />} />
          <Route path="verify-email" element={<VerifyEmail />} />
          <Route path="search-result" element={<SearchResult />} />
        </Route>
        {/* <Route element={<AdminGuard />}> */}
        <Route path="/admin" element={<Admin />}>
          <Route path="product" element={<ProductList />} />
          {/* <Route path="create" element={<ProductCreate />} />
              <Route path=":id" element={<ProductUpdate />} />
          </Route> */}
          <Route path="user" element={<UserList />} />
          <Route path="user-detail/:id" element={<UserDetail />} />
          {/* <Route path="create" element={<UserCreate />} /> */}


          {/* <Route path=":id/update" elem/ent={<UserUpdate />} /> */}
          {/* </Route> */}

          <Route path="order" element={<OrderList />} />
          {/* <Route path=":id" element={<OrderDetail />} />
            </Route> */}
        </Route>
        {/* </Route> */}
      </Routes>
    </div>
  );
}

export default App;
