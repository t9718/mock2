import { Divider, Table } from 'antd';
import { Card } from 'antd';
import { Input } from 'antd';
import { Row, Col } from 'antd';
import { Avatar, Image, Badge, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';

import React, { useState, useEffect } from "react";
import { DeleteOutlined, EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons'
import userApi from '../../api/userApi';
import { Outlet, useParams, Link } from 'react-router-dom';
import { Modal } from 'antd';

const { confirm } = Modal;


function User({ info }) {
    console.log(info)
    return (
        <Row gutter={3}>
            <Col span={4}>
                <span>
                    <Badge dot>
                        {
                            info.avatar ? <Avatar src={info.avatar} shape="square" /> : <Avatar shape="square" icon={<UserOutlined />} />

                        }
                    </Badge>
                </span>
            </Col>
            <Col span={20}>
                <Row >
                    <Col span={13} style={{
                        fontWeight: 'bold',
                        fontSize: 16
                    }}>
                        {info.username}
                    </Col>
                    <Col span={11}>
                        {
                            info.role == 'admin' ? <span style={{
                                backgroundColor: '#4B9528',
                                fontSize: 10,
                                borderRadius: 15,
                                color: '#fff',
                                paddingLeft: 10,
                                paddingRight: 10,
                                paddingTop: 2,
                                paddingBottom: 2
                            }}>Admin</span> : <span style={{
                                backgroundColor: '#4B9528',
                                borderRadius: 15,
                                fontSize: 10,
                                color: '#fff',
                                paddingLeft: 10,
                                paddingRight: 10,
                                paddingTop: 2,
                                paddingBottom: 2
                            }}>Customer</span>
                        }

                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <span>
                            {info.email}
                        </span>
                    </Col>
                </Row>
            </Col>
        </Row>
    )
}

const Search = () => <Input placeholder={`Search product`} />;

function UserList({ itemPerPage = 5 }) {
    const [data, setData] = useState([])
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: itemPerPage,
    })
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        fetch(pagination);
    }, [])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            sorter: true,
            width: '5%',
        },
        {
            title: 'User',
            dataIndex: ['username'],
            sorter: true,
            render: (text, record, index) => (
                <User info={record} />
            ),
            width: '30%'
        },
        {
            title: 'Contact',
            dataIndex: 'contact',
            sorter: true,
            width: '10%',
        },
        {
            title: 'Status',
            dataIndex: 'isActive',
            sorter: true,
            width: '5%',
            render: (text) => (<>
                {text ? 'Active' : 'Disable'}
            </>)
        },
        {
            title: 'Email',
            dataIndex: 'email',
            sorter: true,
    
        },
        {
            title: 'Verify Email',
            dataIndex: 'isEmailVerified',
            sorter: true,
            width: '5%',
            render: (text) => (<span>{text ? 'Yes' : 'No'}</span>)
        },
        {
            title: 'Verify contact',
            dataIndex: 'isContactVerified',
            sorter: true,
            width: '5%',
            render: (text) => (<span>{text ? 'Yes' : 'No'}</span>)
    
        },
        {
            title: '',
            dataIndex: '',
            key: 'x',
            render: (record) => {
                const showConfirm = () => {
                    confirm({
                        title: 'Confirm delete ?',
                        icon: <ExclamationCircleOutlined />,
                        content: `Are you sure to delete product #${record.id}`,
                        onOk() {
                           userApi.deleteUserById(record.id).then((res) => {
                               fetch(pagination)
                           }).catch((error) => {
                               throw Error(error)
                           })
                        },
                        onCancel() {
                            console.log('Cancel');
                        },
                    });
                }
                return (<Button style={{
                    border: 'none'
                }} onClick={showConfirm}><DeleteOutlined style={{ color: 'red' }} />
                </Button>)
    
            },
        },
        {
            title: '',
            dataIndex: '',
            key: 'x',
            render: (record) => {
                const handleEdit = () => {
                    console.log(record)
                }
                return (<Link to={`/admin/user-detail/${record.id}`}>
                    <EditOutlined style={{ color: '#387B18' }} onClick={handleEdit} />
                </Link>)
            },
        },
    ];

    const handleTableChange = (pagination, filters, sorter) => {
        console.log(pagination)
        const pager = { ...pagination };
        pager.current = pagination.current;
        console.log(pager)
        setPagination(pager);
        fetch({
            pageSize: pagination.pageSize,
            current: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            ...filters,
        });
    };

    const fetch = (params = {}) => {
        setLoading(true)
        userApi.queryUser({
            size: itemPerPage,
            page: params.current
        }).then(res => {
            console.log(res.data.result)
            const newpagination = { ...pagination };
            newpagination.total = res.data.total;
            newpagination.pageSize = itemPerPage;
            newpagination.current = res.data.currentPage;
            setLoading(false);
            setData(res.data.result);
            setPagination(newpagination)
        });
    };

    return (
        <>
            <Row style={{
                margin: 20,
                justifyContent: 'space-between'
            }}>
                <Col><span style={{
                    fontSize: 30,
                    fontWeight: 'bold'

                }}>User</span></Col>
                <Col>
                    <Button style={{
                        backgroundColor: '#FFD333',
                        fontWeight: 'bold',
                        fontSize: 18,
                    }}>New User</Button>
                </Col>
            </Row>
            <Card bordered={false} style={{ margin: 20, boxShadow: 'rgb(217 214 214) 0px 0px 20px' }}>
                <Search />
                <Divider />
                <Table
                    columns={columns}
                    rowKey={record => record.id}
                    dataSource={data}
                    pagination={{ ...pagination, position: ['bottomLeft'] }}
                    loading={loading}
                    onChange={handleTableChange}
                />

            </Card>
        </>

    );

}

export default UserList;