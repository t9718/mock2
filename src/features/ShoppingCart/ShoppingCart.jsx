import { Breadcrumb, Button, Skeleton } from "antd";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import cartApi from "../../api/cartApi";
import "./style.scss";
import productPhoto from "../../media/Product Photo.png";
import CustomButton from "../../components/CustomButton/CustomButton";

const ShoppingCart = () => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const token = localStorage.getItem("token");

  useEffect(() => {
    if (token) {
      cartApi.getMyCart().then(({ data }) => {
        setData(data);
        setLoading(false);
      });
    }
  }, [token]);

  return (
    <div>
      <Skeleton loading={loading}>
        <div className="cart-title">
          <Breadcrumb separator=">" style={{ margin: "16px 0", textAlign: "left" }}>
            <Breadcrumb.Item>
              <Link to="/">Home</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Shopping Cart</Breadcrumb.Item>
          </Breadcrumb>
          <h1 className="cart-title__text">Shopping Cart</h1>
        </div>
        <div className="cart-content">
          <div className="cart-item">
            <table className="cart-table">
              <tbody>
                <tr className="cart-table__header">
                  <td className="cart-table__header-title">Image</td>
                  <td className="cart-table__header-title">Product</td>
                  <td className="cart-table__header-title">Price</td>
                  <td className="cart-table__header-title">Quantity</td>
                  <td className="cart-table__header-title">Total</td>
                </tr>
                <tr className="cart-table__body">
                  <td className="cart-table_body-item">
                    <img src={productPhoto} />
                  </td>
                  <td className="cart-table_body-item cart-table__body-text">Adidas Shoes</td>
                  <td className="cart-table_body-item cart-table__body-text">$120.0</td>
                  <td className="cart-table_body-item">
                    <Button type="primary">123</Button>
                  </td>
                  <td className="cart-table_body-item cart-table__body-text">$120.0</td>
                </tr>
                <tr className="cart-table__body">
                  <td className="cart-table_body-item">
                    <img src={productPhoto} />
                  </td>
                  <td className="cart-table_body-item cart-table__body-text">Adidas Shoes</td>
                  <td className="cart-table_body-item cart-table__body-text">$120.0</td>
                  <td className="cart-table_body-item">
                    <Button type="primary">123</Button>
                  </td>
                  <td className="cart-table_body-item cart-table__body-text">$120.0</td>
                </tr>
                <tr className="cart-table__body">
                  <td className="cart-table_body-item">
                    <img src={productPhoto} />
                  </td>
                  <td className="cart-table_body-item cart-table__body-text">Adidas Shoes</td>
                  <td className="cart-table_body-item cart-table__body-text">$120.0</td>
                  <td className="cart-table_body-item">
                    <Button type="primary">123</Button>
                  </td>
                  <td className="cart-table_body-item cart-table__body-text">$120.0</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="cart-calculation">
            <div className="cart-coupon">
              <CustomButton>Coupon Code</CustomButton>
              <CustomButton>Apply Coupon</CustomButton>
            </div>
            <div className="cart-totals">
              <h2 className="cart-totals__title">Cart Totals</h2>
              <div className="cart-totals__items">
                <span className="cart-totals__items-name">Subtotal</span>
                <span className="cart-totals__items-price">$120.00</span>
              </div>
              <div className="cart-totals__items">
                <span className="cart-totals__items-name">Shipping</span>
                <span className="cart-totals__items-price">$20.00</span>
              </div>
              <div className="cart-totals__total">
                <span className="cart-totals__total-name">Total</span>
                <span className="cart-totals__items-price">$140.00</span>
              </div>
              <CustomButton className="cart-checkout-btn">Proceed to checkout</CustomButton>
            </div>
          </div>
        </div>
      </Skeleton>
    </div>
  );
};

export default ShoppingCart;
