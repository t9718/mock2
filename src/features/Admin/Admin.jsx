import { Box } from '@mui/material'
import React from 'react'
import AdminHeader from '../../components/AdminHeader/AdminHeader'
import AdminNavbar from '../../components/AdminNavbar/AdminNavbar'
import { Outlet } from "react-router-dom";

function Admin() {
  return (
    <Box sx={{height: '100vh', width:'100vw', display:'flex'}}>
      <AdminNavbar/>
      <Box sx={{height:'100%', width:'100%', background:'#F5F7FA'}}>
        <AdminHeader/>
        <Outlet />
      </Box>
    </Box>
  )
}

export default Admin