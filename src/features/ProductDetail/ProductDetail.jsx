import { Breadcrumb, Col, Divider, Layout, Menu, Row, Skeleton } from "antd";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import productApi from "../../api/productApi";
import ProductImage from "../../components/ProductImage/ProductImage";
import ProductInfo from "../../components/ProductInfo/ProductInfo";
import ProductReview from "../../components/ProductReview/ProductReview";
import RelatedProducts from "../../components/RelatedProducts/RelatedProducts";
import "./style.scss";

const ProductDetail = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const { id } = useParams();

  useEffect(() => {
    productApi.getById(id).then(({ data }) => {
      setData(data);
      setLoading(false);
    });
  }, [id]);

  return (
    <div>
      <Skeleton loading={loading}>
        <Breadcrumb separator=">" style={{ margin: "16px 24px", textAlign: "left" }}>
          <Breadcrumb.Item>
            <Link to="/">Home</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>{data?.product?.name}</Breadcrumb.Item>
          <Breadcrumb.Item>{data?.product?.brand}</Breadcrumb.Item>
        </Breadcrumb>
      </Skeleton>

      <Skeleton loading={loading}>
        <div className="product-information">
          <Row>
            <Col span={10}>
              <ProductImage product={data?.product?.images} />
            </Col>
            <Col span={14}>
              <ProductInfo product={data?.product} />
            </Col>
          </Row>
        </div>
      </Skeleton>

      <Skeleton loading={loading}>
        <div className="product-review">
          <ProductReview data={data} />
        </div>
      </Skeleton>

      <Skeleton loading={loading}>
        <div className="related-products">
          <RelatedProducts product={data?.product} />
        </div>
      </Skeleton>
    </div>
  );
};

export default ProductDetail;
