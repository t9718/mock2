import React, { useEffect, useState } from 'react'
import { Card, Box, Divider, Grid } from '@mui/material'
import Typography from '@mui/material/Typography';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons'
import CircularProgress from '@mui/material/CircularProgress';
import { useParams } from 'react-router-dom';
import userApi from '../../api/userApi';
import { Row, Col } from 'antd';
import { Avatar, Image, Badge, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';


function UserDetail() {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState({})
    const {id} = useParams()

    useEffect(() => {
        userApi.getUserById(id).then((res) => {
            setLoading(false)
            console.log(res)
            setData(res.data)
        }).catch((error) => {
            console.log(error)
        }).finally(()=> {
            setLoading(false)
        })
    },[id])
    return (
        <>
        <Row style={{
                margin: 20,
                justifyContent: 'space-between'
            }}>
                <Col><span style={{
                    fontSize : 30,
                    fontWeight : 'bold'

                }}>User Detail</span></Col>
                <Col>
                    
                </Col>
            </Row>
            {
                loading ? < CircularProgress /> : <Card style={{
                    margin: 20,
                    boxShadow: 'rgb(217 214 214) 0px 0px 20px',
                    fontFamily: 'Work Sans'
                }}>
                    <Box style={{
                        marginBottom: 20
                    }}>
                        <Box style={{
                            display: 'flex',
                            justifyContent: 'center',
                            marginBottom: 30,
                            marginTop: 30
                        }}>
                            <Box style={{
                                height: 200,
                                width: 200,
                                borderRadius: '50%',
                                overflow: 'hidden'
                            }}>
                                {
                                    data.avatar ? <Avatar style={{height: '100%', width: '100%' }} src={data.avatar} /> : <Avatar style={{height: '100%', width: '100%' }} icon={<UserOutlined />}/>
                                }
                                
                            </Box>
                        </Box>
                        <Box style={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            marginBottom: 30,
                            marginTop: 30
                        }}>
                            <Typography variant="h4" component="div" style={{
                                fontWeight: 'bold'
                            }}>
                               {data.username}
                            </Typography>
                            <Typography component="div" style={{
                                color: '#2234D2'
                            }}>
                                {data.email}
                            </Typography>
                            <Typography component="div">
                                {data.contact}
                            </Typography>
                        </Box>


                    </Box>

                    <Divider />
                    <Box style={{
                        marginTop: 30,
                        marginBottom: 30,
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <Grid container spacing={2} style={{
                            width: 400
                        }}>
                            <Grid item xs={12} container>
                                <Grid item xs={6} style={{
                                    textAlign: 'left',
                                }}>
                                    <Typography>
                                        Role :
                                    </Typography>
                                </Grid>
                                <Grid item xs={6} style={{
                                    textAlign: 'center',
                                }}>
                                    <Typography>
                                        {
                                            data.role == "admin" ? <span style={{
                                                backgroundColor: '#4B9528',
                                                borderRadius: 15,
                                                color: '#fff',
                                                paddingLeft: 10,
                                                paddingRight: 10,
                                                paddingTop: 5,
                                                paddingBottom: 5
                                            }}>Admin</span> :<span style={{
                                                backgroundColor: 'red',
                                                borderRadius: 15,
                                                color: '#fff',
                                                paddingLeft: 10,
                                                paddingRight: 10,
                                                paddingTop: 5,
                                                paddingBottom: 5
                                            }}>Customer</span>
                                        }
                                        
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container>
                                <Grid item xs={6} style={{
                                    textAlign: 'left',
                                }}>
                                    <Typography>
                                        Status :
                                    </Typography>
                                </Grid>
                                <Grid item xs={6} style={{
                                    textAlign: 'center',
                                }}>
                                    <Typography>
                                        {
                                            data.isActive ? <span style={{
                                                backgroundColor: '#4B9528',
                                                borderRadius: 15,
                                                color: '#fff',
                                                paddingLeft: 10,
                                                paddingRight: 10,
                                                paddingTop: 5,
                                                paddingBottom: 5
                                            }}>Active</span> : <span style={{
                                                backgroundColor: 'red',
                                                borderRadius: 15,
                                                color: '#fff',
                                                paddingLeft: 10,
                                                paddingRight: 10,
                                                paddingTop: 5,
                                                paddingBottom: 5
                                            }}>Disable</span>
                                        }
                                        
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container>
                                <Grid item xs={6} style={{
                                    textAlign: 'left',
                                }}>
                                    <Typography>
                                        Verify Email :
                                    </Typography>
                                </Grid>
                                <Grid item xs={6} style={{
                                    textAlign: 'center',
                                }}>
                                    <Typography>
                                        {
                                            data.isEmailVerified ? <CheckOutlined style={{
                                                backgroundColor: '#4B9528',
                                                borderRadius: '50%',
                                                padding: 4,
                                                color: '#fff',
                                            }} /> : <CloseOutlined style={{
                                                backgroundColor: 'red',
                                                borderRadius: '50%',
                                                padding: 4,
                                                color: '#fff',
                                            }} />
                                        }

                                        
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container>
                                <Grid item xs={6} style={{
                                    textAlign: 'left',
                                }}>
                                    <Typography>
                                        Verify Contact :
                                    </Typography>
                                </Grid>
                                <Grid item xs={6} style={{
                                    textAlign: 'center',
                                }}>
                                    <Typography>
                                    {
                                            data.isContactVerified ? <CheckOutlined style={{
                                                backgroundColor: '#4B9528',
                                                borderRadius: '50%',
                                                padding: 4,
                                                color: '#fff',
                                            }} /> : <CloseOutlined style={{
                                                backgroundColor: 'red',
                                                borderRadius: '50%',
                                                padding: 4,
                                                color: '#fff',
                                            }} />
                                        }

                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                </Card>
            }

        </>
    )
}

export default UserDetail