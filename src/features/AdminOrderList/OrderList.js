import { Divider, Table } from 'antd';
import { Card } from 'antd';
import { Input } from 'antd';
import { Row, Col } from 'antd';
import { Avatar, Image, Badge, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';

import React, { useState, useEffect } from "react";
import { DeleteOutlined, EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons'
import orderApi from '../../api/orderApi';
import { Outlet, useParams, Link } from 'react-router-dom';
import { Modal } from 'antd';

const { confirm } = Modal;

const Search = () => <Input placeholder={`Search product`} />;

function OrderList({ itemPerPage = 5 }) {
    const [data, setData] = useState([])
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: itemPerPage,
    })
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        fetch(pagination);
    }, [])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            sorter: true,
            width: '5%',
        },
        {
            title: 'UserID',
            dataIndex: 'userId',
            sorter: true,
            width: '10%'
        },
        {
            title: 'Amount',
            dataIndex: 'totalPrice',
            sorter: true,
            width: '10%',
        },
        {
            title: 'Address',
            dataIndex: 'address',
            sorter: true,
            width: '30%',
            
        },
        {
            title: 'Contact',
            dataIndex: 'contact',
            sorter: true,
    
        },
        {
            title: 'Date',
            dataIndex: 'createdAt',
            sorter: true,
            width: '15%',
            render: (text) => (<span>{text.slice(0,10)}</span>)
        },
        {
            title: 'Pailed',
            dataIndex: 'isPaid',
            sorter: true,
            width: '5%',
            render: (text) => (<>
            {
                text ? <span style={{
                    backgroundColor : '#366AB8',
                    paddingLeft : 7,
                    paddingRight : 7,
                    borderRadius : 10
                }}>Yes</span> :<span style={{
                    backgroundColor : '#FFD333',
                    paddingLeft : 7,
                    paddingRight : 7,
                    borderRadius : 10
                }}>No</span>
            }
            </>
            )
    
        },
        {
            title: 'Status',
            dataIndex: 'status',
            sorter: true,
            width: '5%',
    
        },
        {
            title: '',
            dataIndex: '',
            key: 'x',
            render: (record) => {
                const showConfirm = () => {
                    confirm({
                        title: 'Confirm delete ?',
                        icon: <ExclamationCircleOutlined />,
                        content: `Are you sure to delete product #${record.id}`,
                        onOk() {
                            console.log('Delete');
                        },
                        onCancel() {
                            console.log('Cancel');
                        },
                    });
                }
                return (<Button style={{
                    border: 'none'
                }} onClick={showConfirm}><DeleteOutlined style={{ color: 'red' }} />
                </Button>)
    
            },
        },
        {
            title: '',
            dataIndex: '',
            key: 'x',
            render: (record) => {
                const handleEdit = () => {
                    console.log(record)
                }
                return (<Link to={`/admin/order-detail/${record.id}`}>
                    <EditOutlined style={{ color: '#387B18' }} onClick={handleEdit} />
                </Link>)
            },
        },
    ];

    const handleTableChange = (pagination, filters, sorter) => {
        console.log(pagination)
        const pager = { ...pagination };
        pager.current = pagination.current;
        console.log(pager)
        setPagination(pager);
        fetch({
            pageSize: pagination.pageSize,
            current: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            ...filters,
        });
    };

    const fetch = (params = {}) => {
        setLoading(true)
        orderApi.getOrders({
            size: itemPerPage,
            page: params.current
        }).then(res => {
            console.log(res.data)
            const newpagination = { ...pagination };
            newpagination.total = res.data.orders.total;
            newpagination.pageSize = itemPerPage;
            newpagination.current = res.data.orders.currentPage;
            setLoading(false);
            setData(res.data.orders.result);
            setPagination(newpagination)
        });
    };

    return (
        <>
            <Row style={{
                margin: 20,
                justifyContent: 'space-between'
            }}>
                <Col><span style={{
                    fontSize: 30,
                    fontWeight: 'bold'

                }}>Orders</span></Col>
                <Col>
                    
                </Col>
            </Row>
            <Card bordered={false} style={{ margin: 20, boxShadow: 'rgb(217 214 214) 0px 0px 20px' }}>
                <Search />
                <Divider />
                <Table
                    columns={columns}
                    rowKey={record => record.id}
                    dataSource={data}
                    pagination={{ ...pagination, position: ['bottomLeft'] }}
                    loading={loading}
                    onChange={handleTableChange}
                />

            </Card>
        </>

    );

}

export default OrderList;